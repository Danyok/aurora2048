# Application Template

2048 game for Aurora OS.
Based on https://github.com/MartinBriza/2048.qml
Game was ported on Aurora OS, added interface window,
added some tiles and infinity game mode.

The source code of the project is provided under
[the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md)
documents the rights granted by contributors to the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules
of the Open Mobile Platform which informs you how we expect
the members of the community will interact while contributing and communicating.

For information about contributors see [AUTHORS](AUTHORS.md).

## Project Structure

The project has a common structure
of an application based on C++ and QML for Aurora OS.
