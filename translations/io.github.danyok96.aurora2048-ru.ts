<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="46"/>
        <source>2048
by Danyok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="102"/>
        <source>Win!</source>
        <translation>Победа!</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="106"/>
        <source>Try again?</source>
        <translation>Ещё раз?</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="253"/>
        <source>Score: </source>
        <translation>Счёт: </translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="269"/>
        <source>New</source>
        <translation>Новая</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="288"/>
        <source>Continue</source>
        <translation>Продолжить</translation>
    </message>
</context>
</TS>
